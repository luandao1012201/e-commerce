package com.example.btl_hdv.model

data class Category(val id: Int, val name: String, val image: String)