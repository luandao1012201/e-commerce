package com.example.btl_hdv.model

import com.google.gson.annotations.SerializedName

data class Shoes(
    @SerializedName("id")
    val ID: Int,
    @SerializedName("product_name")
    val name: String,
//    val images: ArrayList<String>,
    @SerializedName("image")
    val images: String,
    val description: String,
//    val size: ArrayList<Int>,
    val price: Float,
    @SerializedName("product_category")
    val category: Category,
//    val brand: String,
    @SerializedName("availability")
    var quantity: Int
)
