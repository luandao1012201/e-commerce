package com.example.btl_hdv.model

import com.google.gson.annotations.SerializedName

data class Cart(
    val id: Int,
    @SerializedName("user_id")
    val userId: String,
    @SerializedName("is_paid")
    val isPaid: Boolean
)