package com.example.btl_hdv.model

data class User(
    var username: String? = "",
    val email: String? = "",
    var image: String? = "",
    var phone: String? = "",
    var address: String? = ""
)
