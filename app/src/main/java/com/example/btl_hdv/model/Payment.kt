package com.example.btl_hdv.model

import com.google.gson.annotations.SerializedName

data class Payment(
    val id: Int,
    @SerializedName("created_date")
    val createdDate: Long,
    val total: Float,
)