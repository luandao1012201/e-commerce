package com.example.btl_hdv.model

data class Shipment(
    val id: Int,
    val name: String,
    val phone: String,
    val address: String,
    val price: Float
)
