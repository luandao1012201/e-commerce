package com.example.btl_hdv.model

import com.google.gson.annotations.SerializedName

data class Order(
    val id: Int,
    @SerializedName("user_id")
    val userId: String,
    @SerializedName("ship_id")
    var shipment: Int,
    @SerializedName("cart_id")
    val cart: Int,
    @SerializedName("payment_id")
    val payment: Int,
    @SerializedName("order_date")
    val orderDate: Long
)

data class OrderResponse(
    var shipment: Shipment,
    val cart: Int,
    val quantity: Int,
    val payment: Payment,
    val orderDate: Long
)
