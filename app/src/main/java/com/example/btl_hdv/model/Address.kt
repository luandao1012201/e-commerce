package com.example.btl_hdv.model

data class Address(val code: Int, val name: String)
