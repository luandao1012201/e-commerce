package com.example.btl_hdv.model

import com.google.gson.annotations.SerializedName

data class CartItem(
    val id: Int,
    @SerializedName("cart")
    val cart: Int,
    @SerializedName("product_id")
    val productId: Int,
    @SerializedName("quantity")
    val quantity: Int,
    @SerializedName("price")
    val price: Float
)