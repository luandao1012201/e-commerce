package com.example.btl_hdv.model.response

import com.google.gson.annotations.SerializedName

data class CustomResponse<T>(
    val status: String,
    @SerializedName("status_code")
    val statusCode: String,
    val data: T
)