package com.example.btl_hdv.network

import com.example.btl_hdv.model.Order
import com.example.btl_hdv.model.Payment
import com.example.btl_hdv.model.response.CustomResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface OrderApiService {
    @POST("/add_order")
    suspend fun addOrder(@Body order: Order): Response<CustomResponse<Int>>

    @GET("/get_order_by_uid/{id}")
    suspend fun getOrder(@Path("id") id: String): Response<CustomResponse<List<Order>>>
}