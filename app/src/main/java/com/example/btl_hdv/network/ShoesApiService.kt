package com.example.btl_hdv.network

import com.example.btl_hdv.model.Category
import com.example.btl_hdv.model.Shoes
import com.example.btl_hdv.model.response.CustomResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ShoesApiService {
    @GET("/shoes/get_all")
    suspend fun getAllShoes(): Response<CustomResponse<List<Shoes>>>

    @GET("/shoes/getshoes/{id}")
    suspend fun getShoes(@Path("id") id: Int): Response<CustomResponse<Shoes>>

    @GET("/shoes/get_category")
    suspend fun getAllCategory(): Response<CustomResponse<List<Category>>>

    @GET("/shoes/get_shoes_by_category/{category}")
    suspend fun getShoesByCategory(@Path("category") category: Int): Response<CustomResponse<List<Shoes>>>

    @GET("/shoes/search_shoes")
    suspend fun searchShoes(@Query("key") key: String): Response<CustomResponse<List<Shoes>>>
}