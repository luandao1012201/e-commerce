package com.example.btl_hdv.network

import com.example.btl_hdv.model.Category
import com.example.btl_hdv.model.Address
import com.example.btl_hdv.model.Payment
import com.example.btl_hdv.model.response.CustomResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface PaymentApiService {
    @GET("/get_province")
    suspend fun getProvince(): Response<CustomResponse<List<Address>>>

    @GET("/get_district/{code}")
    suspend fun getDistrict(@Path("code") code: Int): Response<CustomResponse<List<Address>>>

    @GET("/get_ward/{code}")
    suspend fun getWard(@Path("code") code: Int): Response<CustomResponse<List<Address>>>

    @POST("/add_payment")
    suspend fun addPayment(@Body payment: Payment): Response<CustomResponse<Int>>

    @GET("get_payment/{id}")
    suspend fun getPayment(@Path("id") id: Int): Response<CustomResponse<Payment>>
}