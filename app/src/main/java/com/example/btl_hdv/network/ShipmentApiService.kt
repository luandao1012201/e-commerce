package com.example.btl_hdv.network

import com.example.btl_hdv.model.Payment
import com.example.btl_hdv.model.Shipment
import com.example.btl_hdv.model.response.CustomResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ShipmentApiService {
    @POST("/add_shipment")
    suspend fun addShipment(@Body shipment: Shipment): Response<CustomResponse<Int>>

    @GET("/get_shipment/{id}")
    suspend fun getShipment(@Path("id") id: Int): Response<CustomResponse<Shipment>>
}