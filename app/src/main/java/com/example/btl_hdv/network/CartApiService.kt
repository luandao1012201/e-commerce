package com.example.btl_hdv.network

import com.example.btl_hdv.model.Cart
import com.example.btl_hdv.model.CartItem
import com.example.btl_hdv.model.response.CustomResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query

interface CartApiService {
    @GET("get_cart_by_user/{uid}")
    suspend fun getCartByUser(@Path("uid") uid: String): Response<CustomResponse<Cart>>

    @POST("add_to_cart")
    suspend fun addToCart(@Body cartItem: CartItem): Response<CustomResponse<String>>

    @GET("get_cart_item_by_cart/{cart_id}")
    suspend fun getCartItemByCart(@Path("cart_id") cartId: Int): Response<CustomResponse<List<CartItem>>>

    @PUT("update_pay/{cart_id}")
    suspend fun updatePay(@Path("cart_id") cartId: Int): Response<CustomResponse<String>>

    @GET("delete")
    suspend fun deleteItem(@Query("cart_id") cartId: Int, @Query("product_id") productId: Int): Response<CustomResponse<String>>
}