package com.example.btl_hdv.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

object ApiBuilder {
    private const val API_URL_SHOES = "http://192.168.0.102:3001"
    private const val API_URL_PAYMENT = "http://192.168.0.102:4001"
    private const val API_URL_CART = "http://192.168.0.102:7000"
    private const val API_URL_SHIPMENT = "http://192.168.0.102:5000"
    private const val API_URL_ORDER = "http://192.168.0.102:6000"
    private fun provideRetrofit(url: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val shoesApiService: ShoesApiService by lazy {
        provideRetrofit(API_URL_SHOES).create(ShoesApiService::class.java)
    }
    val paymentApiService: PaymentApiService by lazy {
        provideRetrofit(API_URL_PAYMENT).create(PaymentApiService::class.java)
    }
    val cartApiService: CartApiService by lazy {
        provideRetrofit(API_URL_CART).create(CartApiService::class.java)
    }
    val shipmentApiService: ShipmentApiService by lazy {
        provideRetrofit(API_URL_SHIPMENT).create(ShipmentApiService::class.java)
    }
    val orderApiService: OrderApiService by lazy {
        provideRetrofit(API_URL_ORDER).create(OrderApiService::class.java)
    }
}