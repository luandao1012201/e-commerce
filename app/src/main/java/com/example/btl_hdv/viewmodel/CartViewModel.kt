package com.example.btl_hdv.viewmodel

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.btl_hdv.model.Cart
import com.example.btl_hdv.model.CartItem
import com.example.btl_hdv.model.Shoes
import com.example.btl_hdv.repository.CartRepository
import com.example.btl_hdv.repository.ShoesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CartViewModel : ViewModel() {
    private val cartRepository by lazy { CartRepository() }
    private val shoesRepository by lazy { ShoesRepository() }
    var cartId = MutableLiveData<Cart?>()
    var listShoesInCart = MutableLiveData<List<Shoes?>>()
    var shoesQuantity = MutableLiveData(1)
    fun getCartByUser(uid: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = cartRepository.getCartByUser(uid)
            if (response.isSuccessful) {
                withContext(Dispatchers.Main) {
                    cartId.value = response.body()?.data
                }
            }
        }
    }

    fun changeQuantity(isIncrease: Boolean, context: Context) {
        if (isIncrease) {
            shoesQuantity.value = shoesQuantity.value?.plus(1)
        } else {
            if (shoesQuantity.value == 1) {
                Toast.makeText(context, "Số lượng phải lớn hơn 1", Toast.LENGTH_SHORT).show()
            } else {
                shoesQuantity.value = shoesQuantity.value?.minus(1)
            }
        }
    }

    fun addToCart(cartItem: CartItem, context: Context) {
        try {
            viewModelScope.launch(Dispatchers.IO) {
                val response = cartRepository.addToCart(cartItem)
                val message = if (response.isSuccessful)
                    "Thêm vào giỏ hàng thành công"
                else
                    "Thêm vào giỏ hàng thất bại"
                withContext(Dispatchers.Main) {
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                }
            }
        } catch (e: Exception) {
            Log.d("test123", e.message.toString())
        }
    }


    fun getCartItemByCart(cartId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = cartRepository.getCartItemByCart(cartId)
            if (response.isSuccessful) {
                val listCartItem = response.body()?.data
                val listItem = mutableListOf<Shoes?>()
                listCartItem?.forEach {
                    val shoes = shoesRepository.getShoes(it.productId).body()?.data
                    shoes?.quantity = it.quantity
                    listItem += shoes
                }
                withContext(Dispatchers.Main) {
                    listShoesInCart.value = listItem
                }
            }
        }
    }

    fun updatePay(cartId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            cartRepository.updatePay(cartId)
        }
    }

    fun deleteItem(context: Context, cartId: Int, productId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = cartRepository.deleteItem(cartId, productId)
                if (response.isSuccessful) {
                    getCartItemByCart(cartId)
                    Toast.makeText(context, "Xóa thành công", Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {

            }
        }
    }
}