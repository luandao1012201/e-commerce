package com.example.btl_hdv.viewmodel

import android.app.Activity
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.btl_hdv.repository.AuthRepository
import com.facebook.CallbackManager
import com.facebook.login.LoginResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AuthViewModel : ViewModel() {
    private val authRepository by lazy { AuthRepository() }
    var authSuccess = MutableLiveData<Pair<Boolean, String>>()

    fun register(username: String, email: String, password: String) {
        viewModelScope.launch(Dispatchers.IO) {
            authSuccess.postValue(authRepository.register(username, email, password))
        }
    }

    fun login(email: String, password: String) {
        viewModelScope.launch(Dispatchers.IO) {
            authSuccess.postValue(authRepository.login(email, password))
        }
    }

    fun loginWithGoogle(idToken: String, username: String, email: String, image: Uri) {
        viewModelScope.launch(Dispatchers.IO) {
            authSuccess.postValue(authRepository.loginWithGoogle(idToken, username, email, image))
        }
    }

    fun loginWithFacebook(loginResult: LoginResult) {
        viewModelScope.launch(Dispatchers.IO) {
            authSuccess.postValue(authRepository.loginWithFacebook(loginResult))
        }
    }

    fun loginWithTwitter(activity: Activity) {
        viewModelScope.launch(Dispatchers.IO) {
            authSuccess.postValue(authRepository.loginWithTwitter(activity))
        }
    }
}