package com.example.btl_hdv.viewmodel

import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.btl_hdv.model.User
import com.example.btl_hdv.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserViewModel : ViewModel() {
    private val userRepository by lazy { UserRepository() }
    var currentUser = MutableLiveData<User?>()
    var updateUserResponse = MutableLiveData<Pair<Boolean, String>>()
    var changePassword = MutableLiveData<Pair<Boolean, String>>()
    fun getUserData() {
        viewModelScope.launch(Dispatchers.IO) {
            val user = userRepository.getDataUser()
            currentUser.postValue(user)
        }
    }

    fun updateProfile(uri: Uri, user: User) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val image = userRepository.uploadImage(uri)
                user.image = image
                updateUserResponse.postValue(userRepository.updateUser(user))
            } catch (e: Exception) {
            }
        }
    }

    fun changePassword(oldPassword: String, newPassword: String) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                changePassword.postValue(userRepository.changePassword(oldPassword, newPassword))
            } catch (e: Exception) {
                Log.d("test123", e.message.toString())
            }
        }
    }
}