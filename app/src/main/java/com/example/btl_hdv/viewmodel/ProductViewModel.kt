package com.example.btl_hdv.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.btl_hdv.model.Category
import com.example.btl_hdv.model.Shoes
import com.example.btl_hdv.repository.ShoesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProductViewModel : ViewModel() {
    private val shoesRepository by lazy { ShoesRepository() }
    var listShoes = MutableLiveData<List<Shoes>?>()
    var shoes = MutableLiveData<Shoes?>()
    var listCategory = MutableLiveData<List<Category>?>()

    fun getAllProduct() {
        viewModelScope.launch(Dispatchers.IO) {
            val response = shoesRepository.getAllProduct()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    listShoes.value = response.body()?.data
                }
            }
        }
    }

    fun getShoes(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = shoesRepository.getShoes(id)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    shoes.postValue(response.body()?.data)
                }
            }
        }
    }

    fun getAllCategory() {
        viewModelScope.launch(Dispatchers.IO) {
            val response = shoesRepository.getAllCategory()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful)
                    listCategory.value = response.body()?.data
            }
        }
    }

    fun getShoesByCategory(category: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = shoesRepository.getShoesByCategory(category)
                if (response.isSuccessful) {
                    withContext(Dispatchers.Main) {
                        listShoes.value = response.body()?.data
                    }
                }
            } catch (e: Exception) {
                Log.d("test123", e.message.toString())
            }
        }
    }

    fun searchShoes(key: String) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = shoesRepository.searchShoes(key)
                if (response.isSuccessful) {
                    withContext(Dispatchers.Main) {
                        listShoes.value = response.body()?.data
                    }
                }
            } catch (e: Exception) {
                Log.d("test123", e.message.toString())
            }

        }
    }
}