package com.example.btl_hdv.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.btl_hdv.model.Address
import com.example.btl_hdv.model.Order
import com.example.btl_hdv.model.OrderResponse
import com.example.btl_hdv.model.Payment
import com.example.btl_hdv.model.Shipment
import com.example.btl_hdv.repository.CartRepository
import com.example.btl_hdv.repository.OrderRepository
import com.example.btl_hdv.repository.PaymentRepository
import com.example.btl_hdv.repository.ShipmentRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.security.cert.Extension

class PaymentViewModel : ViewModel() {
    private val paymentRepository by lazy { PaymentRepository() }
    private val orderRepository by lazy { OrderRepository() }
    private val shipmentRepository by lazy { ShipmentRepository() }
    private val cartRepository by lazy { CartRepository() }
    var listProvince = MutableLiveData<List<Address>?>()
    var listDistrict = MutableLiveData<List<Address>?>()
    var listWard = MutableLiveData<List<Address>?>()
    var paymentId = MutableLiveData<Int?>()
    var shipmentId = MutableLiveData<Int?>()
    var listOrderResponse = MutableLiveData<List<OrderResponse>>()
    fun getAllProvince() {
        viewModelScope.launch(Dispatchers.IO) {
            val response = paymentRepository.getAllProvince()
            if (response.isSuccessful) {
                withContext(Dispatchers.Main) {
                    listProvince.value = response.body()?.data
                }
            }
        }
    }

    fun getAllDistrict(code: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = paymentRepository.getAllDistrict(code)
            if (response.isSuccessful) {
                withContext(Dispatchers.Main) {
                    listDistrict.value = response.body()?.data
                }
            }
        }
    }

    fun getAllWard(code: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = paymentRepository.getAllWard(code)
            if (response.isSuccessful) {
                withContext(Dispatchers.Main) {
                    listWard.value = response.body()?.data
                }
            }
        }
    }

    fun addPayment(payment: Payment) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = paymentRepository.addPayment(payment)
                if (response.isSuccessful) {
                    withContext(Dispatchers.Main) {
                        paymentId.value = response.body()?.data
                    }
                }
            } catch (e: Exception) {
                Log.d("test123", e.message.toString())
            }
        }
    }

    fun addShipment(shipment: Shipment) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = shipmentRepository.addShipment(shipment)
                if (response.isSuccessful) {
                    withContext(Dispatchers.Main) {
                        shipmentId.value = response.body()?.data
                    }
                }
            } catch (e: Exception) {
                Log.d("test123", e.message.toString())
            }
        }
    }

    fun addOrder(order: Order) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = orderRepository.addOrder(order)
                if (response.isSuccessful) {
                    Log.d("test123", "Success")
                }
            } catch (e: Exception) {
                Log.d("test123", e.message.toString())
            }
        }
    }

    fun getOrder(id: String) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val list = mutableListOf<OrderResponse>()
                val orders = orderRepository.getOrder(id).body()?.data
                orders?.forEach { order ->
                    val payment = paymentRepository.getPayment(order.payment).body()?.data
                    val shipment = shipmentRepository.getShipment(order.shipment).body()?.data
                    val quantity = cartRepository.getCartItemByCart(order.cart).body()?.data?.size
                    val orderResponse =
                        OrderResponse(
                            shipment!!,
                            order.cart,
                            quantity!!,
                            payment!!,
                            order.orderDate
                        )
                    list += orderResponse
                }
                listOrderResponse.postValue(list)
            } catch (e: Exception) {
                Log.d("test123", e.message.toString())
            }
        }
    }
}