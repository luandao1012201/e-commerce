package com.example.btl_hdv.ui.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import com.example.btl_hdv.R
import com.example.btl_hdv.databinding.FragmentLoginBinding
import com.example.btl_hdv.ui.activities.HomeActivity
import com.example.btl_hdv.viewmodel.AuthViewModel
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException


class LoginFragment : Fragment() {
    companion object {
        private const val RC_SIGN_IN = 12345
    }

    private lateinit var binding: FragmentLoginBinding
    private val authViewModel: AuthViewModel by viewModels()
    private val callbackManager by lazy { CallbackManager.Factory.create() }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initListeners() {
        setFragmentResultListener(SignupFragment.INFO_SIGNUP) { _, bundle ->
            val email = bundle.getString(SignupFragment.EMAIL_SIGNUP).toString()
            val password = bundle.getString(SignupFragment.PASSWORD_SIGNUP).toString()
            if (email.isNotEmpty() && password.isNotEmpty()) {
                binding.edtEmail.setText(email)
                binding.edtPassword.setText(password)
            }
        }
        binding.tvSignup.setOnClickListener {
            activity?.supportFragmentManager?.commit {
                add(R.id.fragment_container_view, SignupFragment())
                addToBackStack(null)
            }
        }
        binding.btnLogin.setOnClickListener {
            val email = binding.edtEmail.text.toString().trim()
            val password = binding.edtPassword.text.toString().trim()
            authViewModel.login(email, password)
        }
        binding.ivGoogle.setOnClickListener {
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .requestProfile()
                .build()
            val googleSignInClient = GoogleSignIn.getClient(requireActivity(), gso)
            val signInIntent = googleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }
        binding.ivFacebook.setOnClickListener {
            LoginManager.getInstance().logInWithReadPermissions(this, listOf("public_profile","email"))
        }
        binding.ivTwitter.setOnClickListener {
            authViewModel.loginWithTwitter(activity as Activity)
        }
        LoginManager.getInstance()
            .registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onCancel() = Unit

                override fun onError(error: FacebookException) = Unit

                override fun onSuccess(result: LoginResult) {
                    authViewModel.loginWithFacebook(result)
                }
            })
    }

    private fun initViews() {
        authViewModel.authSuccess.observe(viewLifecycleOwner) {
            if (it.first) {
                activity?.startActivity(Intent(activity, HomeActivity::class.java))
                activity?.finish()
            }
            Toast.makeText(activity?.applicationContext, it.second, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN && resultCode == Activity.RESULT_OK) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                val idToken = account?.idToken
                val username = account?.displayName
                val email = account?.email
                val image = account?.photoUrl
                if (idToken != null && username !== null && email != null && image != null)
                    authViewModel.loginWithGoogle(idToken, username, email, image)
            } catch (e: ApiException) {
                Toast.makeText(requireContext(), "Google sign in failed", Toast.LENGTH_SHORT).show()
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }
}