package com.example.btl_hdv.ui.fragments

import android.app.AlertDialog
import android.content.Context.MODE_PRIVATE
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.btl_hdv.databinding.FragmentCartBinding
import com.example.btl_hdv.model.Shoes
import com.example.btl_hdv.ui.adapters.ItemCartAdapter
import com.example.btl_hdv.ultis.showBottomSheet
import com.example.btl_hdv.viewmodel.CartViewModel
import com.example.btl_hdv.viewmodel.PaymentViewModel
import com.example.btl_hdv.viewmodel.UserViewModel
import com.example.btl_hdv.zalopay.Api.CreateOrder
import com.example.btl_hdv.zalopay.Constant.AppInfo
import com.vnpay.authentication.VNP_AuthenticationActivity
import org.json.JSONException
import org.json.JSONObject
import vn.momo.momo_partner.AppMoMoLib
import vn.zalopay.sdk.Environment
import vn.zalopay.sdk.ZaloPayError
import vn.zalopay.sdk.ZaloPaySDK
import vn.zalopay.sdk.listeners.PayOrderListener
import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.TreeMap
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec


class CartFragment : Fragment() {
    companion object {
        const val vnp_HashSecret = "JOZNEWQXOOGMOTMKTODZAFBPOVGHNEEW"
        const val vnp_TmnCode = "W5OYK9RR"
        const val ORDER_INFO = "ORDER_INFO"
        const val NAME = "NAME"
        const val PHONE = "PHONE"
        const val PRICE = "PRICE"
        const val ADDRESS = "ADDRESS"
        const val CART = "CART"
    }

    private val sharedPreferences by lazy {
        context?.getSharedPreferences(
            "Preferences",
            MODE_PRIVATE
        )
    }
    private var totalPrice = 0f
    private val paymentViewModel: PaymentViewModel by activityViewModels()
    private val cartViewModel: CartViewModel by activityViewModels()
    private val userViewModel: UserViewModel by activityViewModels()
    private var adapter: ItemCartAdapter? = null
    private lateinit var binding: FragmentCartBinding
    private val fee = "0"
    var environment = 0 //developer default

    private val merchantName = "HoangNgoc"
    private val merchantCode = "MOMOC2IC20220510"
    private val merchantNameLabel = "Luan"
    private val description = "Thanh toán dịch vụ ABC"
    private val calendar = Calendar.getInstance()
    private val formatter by lazy { SimpleDateFormat("yyyyMMddHHmmss") }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initViews() {
        val policy = ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        ZaloPaySDK.tearDown();
        ZaloPaySDK.init(AppInfo.APP_ID, Environment.SANDBOX);
        AppMoMoLib.getInstance()
            .setEnvironment(AppMoMoLib.ENVIRONMENT.DEVELOPMENT); // AppMoMoLib.ENVIRONMENT.PRODUCTION
        adapter = ItemCartAdapter()
        cartViewModel.cartId.observe(this.viewLifecycleOwner) {
            it?.id?.let { it1 -> cartViewModel.getCartItemByCart(it1) }
        }
        cartViewModel.listShoesInCart.observe(this.viewLifecycleOwner) {
            adapter?.setData(it as List<Shoes>)
            binding.rcvItem.adapter = adapter
            totalPrice = 15000f
            it.forEach { shoes -> totalPrice += shoes?.price!! * shoes.quantity!! }
        }
    }

    private fun initListeners() {
        adapter?.setDeleteItem {
            val builder = AlertDialog.Builder(this.requireContext())
            builder.setMessage("Xác nhận xóa")
                .setPositiveButton("Có") { _, _ ->
                    cartViewModel.deleteItem(
                        this.requireContext(),
                        cartViewModel.cartId.value?.id!!,
                        it.ID
                    )
                }
                .setNegativeButton("Không") { _, _ -> }
            builder.create().show()
        }
        binding.cv.setOnClickListener {
            context?.showBottomSheet(
                userViewModel,
                paymentViewModel,
                totalPrice
            ) { method, address, phone ->
                sharedPreferences?.edit()?.apply {
                    putString(NAME, userViewModel.currentUser.value?.username)
                    putString(PHONE, phone)
                    putString(ADDRESS, address)
                    putFloat(PRICE, totalPrice)
                    putInt(CART, cartViewModel.cartId.value?.id!!)
                }?.apply()
                when (method) {
                    "Momo" -> requestPaymentMomo()
                    "VNPay" -> requestPaymentVNPay()
                    "Zalo pay" -> requestZalopay()
                }
            }
        }
    }

    private fun requestZalopay() {
        val orderApi = CreateOrder()
        try {
            val data = orderApi.createOrder("100000")
            val code = data!!.getString("returncode")
            if (code == "1") {
                val token = data.getString("zptranstoken")
                ZaloPaySDK.getInstance().payOrder(
                    requireActivity(),
                    token,
                    "orderresultactivity://sdk",
                    object : PayOrderListener {
                        override fun onPaymentSucceeded(p0: String?, p1: String?, p2: String?) {
                            Log.d("test123", "onPaymentSucceeded: ")
                        }

                        override fun onPaymentCanceled(p0: String?, p1: String?) {
                            Log.d("test123", "onPaymentCanceled: ")
                        }

                        override fun onPaymentError(p0: ZaloPayError?, p1: String?, p2: String?) {
                            Log.d("test123", "onPaymentError: ")
                        }
                    })

            }
        } catch (e: Exception) {
            Log.d("test123", e.message.toString())
        }
    }

    private fun generateVnpSecureHash(params: Map<String, String>, secretKey: String): String {
        val sortedParams = TreeMap(params)
        val data = StringBuilder()
        for ((key, value) in sortedParams) {
            if (!value.isNullOrBlank()) {
                data.append(key).append("=").append(value).append("&")
            }
        }
        data.deleteCharAt(data.length - 1)
        val secretKeySpec =
            SecretKeySpec(secretKey.toByteArray(StandardCharsets.UTF_8), "HmacSHA512")
        val mac = Mac.getInstance("HmacSHA512")
        mac.init(secretKeySpec)
        val hashBytes = mac.doFinal(data.toString().toByteArray(StandardCharsets.UTF_8))
        val hexString = StringBuilder()
        for (b in hashBytes) {
            hexString.append(String.format("%02x", b))
        }
        return hexString.toString()
    }

    private fun requestPaymentVNPay() {
        val intent = Intent(activity, VNP_AuthenticationActivity::class.java)
        val params = mapOf(
            "vnp_Amount" to (totalPrice.toInt() * 100).toString(),
            "vnp_Command" to "pay",
            "vnp_CreateDate" to formatter.format(calendar.time),
            "vnp_CurrCode" to "VND",
            "vnp_IpAddr" to "192.168.0.103",
            "vnp_Locale" to "vn",
            "vnp_OrderInfo" to URLEncoder.encode("Thanh toan don hang", "UTF-8"),
            "vnp_OrderType" to "other",
            "vnp_ReturnUrl" to URLEncoder.encode("orderresultactivity://sdk", "UTF-8"),
            "vnp_TmnCode" to vnp_TmnCode,
            "vnp_TxnRef" to calendar.timeInMillis.toString(),
            "vnp_Version" to "2.1.0",
        )
        val secretKey = vnp_HashSecret
        val secureHash = generateVnpSecureHash(params, secretKey)

        val url = StringBuilder("https://sandbox.vnpayment.vn/paymentv2/vpcpay.html?")
        for ((key, value) in params) {
            if (!value.isNullOrBlank()) {
                url.append(key).append("=").append(value).append("&")
            }
        }
        url.append("vnp_SecureHash=").append(secureHash)
        intent.putExtra("url", url.toString())
        intent.putExtra("tmn_code", vnp_TmnCode)
        intent.putExtra("scheme", "orderresultactivity")
        intent.putExtra("is_sandbox", true)
        activity?.startActivity(intent)
    }

    //Get token through MoMo app
    private fun requestPaymentMomo() {
        AppMoMoLib.getInstance().setAction(AppMoMoLib.ACTION.PAYMENT)
        AppMoMoLib.getInstance().setActionType(AppMoMoLib.ACTION_TYPE.GET_TOKEN)
        val eventValue: MutableMap<String, Any> = HashMap()
        //client Required
        eventValue["merchantname"] =
            merchantName //Tên đối tác. được đăng ký tại https://business.momo.vn. VD: Google, Apple, Tiki , CGV Cinemas
        eventValue["merchantcode"] =
            merchantCode //Mã đối tác, được cung cấp bởi MoMo tại https://business.momo.vn
        eventValue["amount"] = totalPrice.toInt() //Kiểu integer
        eventValue["orderId"] =
            "orderId123456789" //uniqueue id cho Bill order, giá trị duy nhất cho mỗi đơn hàng
        eventValue["orderLabel"] = "Mã đơn hàng" //gán nhãn

        //client Optional - bill info
        eventValue["merchantnamelabel"] = "Dịch vụ" //gán nhãn
        eventValue["fee"] = "0"//Kiểu integer
        eventValue["description"] = description //mô tả đơn hàng - short description

        //client extra data
        eventValue["requestId"] = merchantCode + "merchant_billId_" + System.currentTimeMillis()
        eventValue["partnerCode"] = merchantCode
        //Example extra data
        val objExtraData = JSONObject()
        try {
            objExtraData.put("site_code", "008")
            objExtraData.put("site_name", "CGV Cresent Mall")
            objExtraData.put("screen_code", 0)
            objExtraData.put("screen_name", "Special")
            objExtraData.put("movie_name", "Kẻ Trộm Mặt Trăng 3")
            objExtraData.put("movie_format", "2D")
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        eventValue["extraData"] = objExtraData.toString()
        eventValue["extra"] = ""
        AppMoMoLib.getInstance().requestMoMoCallBack(activity, eventValue)
    }
}