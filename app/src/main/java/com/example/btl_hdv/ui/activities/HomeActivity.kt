package com.example.btl_hdv.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.forEach
import androidx.fragment.app.activityViewModels
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.btl_hdv.R
import com.example.btl_hdv.databinding.ActivityHomeBinding
import com.example.btl_hdv.ui.adapters.ViewPagerAdapter
import com.example.btl_hdv.ui.fragments.CartFragment
import com.example.btl_hdv.viewmodel.CartViewModel
import com.example.btl_hdv.viewmodel.UserViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import vn.momo.momo_partner.AppMoMoLib
import vn.zalopay.sdk.ZaloPaySDK

class HomeActivity : AppCompatActivity() {
    private val binding by lazy { ActivityHomeBinding.inflate(layoutInflater) }
    private val cartViewModel: CartViewModel by viewModels()
    private val userViewModel: UserViewModel by viewModels()

    //    private var cartId = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initViews()
        initListeners()
    }

    private fun initViews() {
        setViewPager()
        cartViewModel.getCartByUser(Firebase.auth.currentUser?.uid!!)
        userViewModel.getUserData()
    }

    override fun onStart() {
        super.onStart()
        Log.d("test123", "onStart: ")
        cartViewModel.getCartByUser(Firebase.auth.currentUser?.uid!!)
    }

    private fun initListeners() {
        binding.vp.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) = Unit

            override fun onPageSelected(position: Int) {
                binding.bottomNav.menu.forEach {
                    it.isChecked = false
                }
                when (position) {
                    0 -> binding.bottomNav.menu.findItem(R.id.shop).isChecked = true
                    1 -> binding.bottomNav.menu.findItem(R.id.explore).isChecked = true
                    2 -> binding.bottomNav.menu.findItem(R.id.cart).isChecked = true
//                    3 -> binding.bottomNav.menu.findItem(R.id.favourite).isChecked = true
                    3 -> binding.bottomNav.menu.findItem(R.id.account).isChecked = true
                }
            }

            override fun onPageScrollStateChanged(state: Int) = Unit

        })
    }

    private fun setViewPager() {
        var adapter = ViewPagerAdapter(supportFragmentManager, 5)
        binding.vp.adapter = adapter
        binding.bottomNav.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.shop -> {
                    binding.vp.currentItem = 0
                    true
                }

                R.id.explore -> {
                    binding.vp.currentItem = 1
                    true
                }

                R.id.cart -> {
                    binding.vp.currentItem = 2
                    true
                }

//                R.id.favourite -> {
//                    binding.vp.currentItem = 3
//                    true
//                }

                R.id.account -> {
                    binding.vp.currentItem = 3
                    true
                }

                else -> false
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AppMoMoLib.getInstance().REQUEST_CODE_MOMO && resultCode == -1) {
            if (data != null) {
                if (data.getIntExtra("status", -1) == 0) {
                    val token = data.getStringExtra("data") //Token response
                    if (token != null && token != "") {
                        orderResult(true)
                    } else {
                        orderResult(false)
                    }
                } else if (data.getIntExtra("status", -1) == 1) {
                    orderResult(false)
                } else if (data.getIntExtra("status", -1) == 2) {
                    orderResult(false)
                } else {
                    orderResult(false)
                }
            } else {
                orderResult(false)
            }
        } else {
            orderResult(false)
        }
    }

    private fun orderResult(success: Boolean) {
        val intent = Intent(this, OrderResultActivity::class.java)
        intent.putExtra("success", success)
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("test123", "onDestroy: ")
    }

    override fun onPause() {
        super.onPause()
        Log.d("test123", "onPause: ")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("test123", "onRestart: ")
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        ZaloPaySDK.getInstance().onResult(intent)
    }
}