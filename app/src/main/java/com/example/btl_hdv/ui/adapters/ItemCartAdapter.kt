package com.example.btl_hdv.ui.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.btl_hdv.databinding.ItemCartFragmentBinding
import com.example.btl_hdv.model.Shoes
import com.example.btl_hdv.ultis.format
import com.example.btl_hdv.ultis.loadImage

@SuppressLint("NotifyDataSetChanged")
class ItemCartAdapter : Adapter<ItemCartAdapter.ItemCartViewHolder>() {
    private var listShoes = arrayListOf<Shoes>()
    private var callbackDelete: ((shoes: Shoes) -> Unit)? = null
    fun setData(list: List<Shoes>) {
        listShoes = list as ArrayList<Shoes>
        notifyDataSetChanged()
    }

    fun setDeleteItem(callback: (shoes: Shoes) -> Unit) {
        callbackDelete = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemCartViewHolder {
        val itemCartViewHolder = ItemCartViewHolder(
            ItemCartFragmentBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
        itemCartViewHolder.bindListeners()
        return itemCartViewHolder
    }

    override fun getItemCount(): Int {
        return listShoes.size
    }

    override fun onBindViewHolder(holder: ItemCartViewHolder, position: Int) {
        holder.bind(position)
    }

    @SuppressLint("SetTextI18n")
    inner class ItemCartViewHolder(private val binding: ItemCartFragmentBinding) :
        ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.tvNameItemCard.text = listShoes[position].name
            binding.tvPrice.text = listShoes[position].price.format()
            binding.ivItemCart.loadImage(listShoes[position].images)
            binding.tvCartQuantity.text = listShoes[position].quantity.toString()
            binding.tvNameItemCard.isSelected = true
            binding.tvPrice.isSelected = true
        }

        fun bindListeners() {
            binding.imgDelete.setOnClickListener {
                if (adapterPosition != -1) {
                    callbackDelete?.invoke(listShoes[adapterPosition])
                }
            }
        }
    }
}