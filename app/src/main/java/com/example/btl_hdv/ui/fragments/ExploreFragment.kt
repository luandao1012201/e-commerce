package com.example.btl_hdv.ui.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.btl_hdv.databinding.FragmentExploreBinding
import com.example.btl_hdv.model.Category
import com.example.btl_hdv.ui.activities.SearchActivity
import com.example.btl_hdv.ui.adapters.ItemExploreAdapter
import com.example.btl_hdv.viewmodel.ProductViewModel

class ExploreFragment : Fragment() {
    private lateinit var binding: FragmentExploreBinding
    private var itemExploreAdapter: ItemExploreAdapter? = null
    private val productViewModel: ProductViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentExploreBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initViews() {
        itemExploreAdapter = ItemExploreAdapter()
        binding.rv.adapter = itemExploreAdapter
        productViewModel.listCategory.observe(this.viewLifecycleOwner) {
            itemExploreAdapter?.setData(it as ArrayList<Category>)
        }
    }

    private fun initListeners() {
        productViewModel.getAllCategory()
        itemExploreAdapter?.setOnClick {
            val intent = Intent(activity, SearchActivity::class.java)
            intent.putExtra("category_id", it)
            activity?.startActivity(intent)
        }
    }
}