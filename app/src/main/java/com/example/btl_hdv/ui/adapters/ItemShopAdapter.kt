package com.example.btl_hdv.ui.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.btl_hdv.databinding.ItemShopFragmentBinding
import com.example.btl_hdv.model.Shoes
import com.example.btl_hdv.ultis.format
import com.example.btl_hdv.ultis.loadImage

@SuppressLint("NotifyDataSetChanged")
class ItemShopAdapter : Adapter<ItemShopAdapter.ItemViewHolder>() {
    private var listShoes = arrayListOf<Shoes>()
    private var onClickCallback: ((position: Int) -> Unit)? = null
    private var onClickAddToCartCallback: ((shoes: Shoes) -> Unit)? = null
    fun setData(list: List<Shoes>) {
        listShoes = list as ArrayList<Shoes>
        notifyDataSetChanged()
    }

    fun setItemOnClick(callback: ((position: Int) -> Unit)? = null) {
        onClickCallback = callback
    }

    fun addToCart(callback: ((shoes: Shoes) -> Unit)? = null) {
        onClickAddToCartCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemViewHolder = ItemViewHolder(
            ItemShopFragmentBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
        itemViewHolder.bindListeners()
        return itemViewHolder
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int = listShoes.size

    @SuppressLint("SetTextI18n")
    inner class ItemViewHolder(private val binding: ItemShopFragmentBinding) :
        ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.tvNameItemGrid.text = listShoes[position].name
            binding.tvPrice.text = listShoes[position].price.format()
            binding.tv.text = listShoes[position].category.name
            binding.ivItemGrid.loadImage(listShoes[position].images)
            binding.tvNameItemGrid.isSelected = true
            binding.tvPrice.isSelected = true
        }

        fun bindListeners() {
            binding.root.setOnClickListener {
                onClickCallback?.invoke(listShoes[adapterPosition].ID)
            }
            binding.btnAddToBasket.setOnClickListener {
                onClickAddToCartCallback?.invoke(listShoes[adapterPosition])
            }
        }
    }
}