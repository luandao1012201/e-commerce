package com.example.btl_hdv.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.activity.viewModels
import com.example.btl_hdv.databinding.ActivitySearchBinding
import com.example.btl_hdv.ui.adapters.ItemExploreAdapter
import com.example.btl_hdv.ui.adapters.ItemShopAdapter
import com.example.btl_hdv.viewmodel.ProductViewModel

class SearchActivity : AppCompatActivity() {
    private val binding by lazy { ActivitySearchBinding.inflate(layoutInflater) }
    private val productViewModel: ProductViewModel by viewModels()
    private var adapter: ItemShopAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initViews()
        initListeners()
    }

    private fun initViews() {
        adapter = ItemShopAdapter()
        val intent = intent
        val category = intent.getIntExtra("category_id", -1)
        if (category != -1) {
            productViewModel.getShoesByCategory(category)
        }
    }

    private fun initListeners() {
        binding.ivBack.setOnClickListener {
            onBackPressedDispatcher.onBackPressed()
        }
        binding.tvSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                productViewModel.searchShoes(p0.toString())
            }

            override fun afterTextChanged(p0: Editable?) = Unit
        })

        productViewModel.listShoes.observe(this) {
            if (it != null) {
                adapter?.setData(it)
                binding.rcvItem.adapter = adapter
            }
        }
    }
}