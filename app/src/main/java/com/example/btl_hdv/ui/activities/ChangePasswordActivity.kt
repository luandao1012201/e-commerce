package com.example.btl_hdv.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import com.example.btl_hdv.R
import com.example.btl_hdv.databinding.ActivityChangePasswordBinding
import com.example.btl_hdv.viewmodel.UserViewModel

class ChangePasswordActivity : AppCompatActivity() {
    private val binding by lazy { ActivityChangePasswordBinding.inflate(layoutInflater) }
    private val userViewModel: UserViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initViews()
        initListeners()
    }

    private fun initViews() {
        userViewModel.changePassword.observe(this) {
            if (it.first) {
                onBackPressedDispatcher.onBackPressed()
            }
            Toast.makeText(applicationContext, it.second, Toast.LENGTH_SHORT).show()
        }
    }

    private fun initListeners() {
        binding.btnComfirm.setOnClickListener {
            val oldPassword = binding.edtOldPassword.text.toString()
            val newPassword = binding.edtNewPassword.text.toString()
            userViewModel.changePassword(oldPassword, newPassword)
        }
    }
}