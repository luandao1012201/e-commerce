package com.example.btl_hdv.ui.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.btl_hdv.databinding.ActivityEditProfileBinding
import com.example.btl_hdv.ultis.loadImage
import com.example.btl_hdv.viewmodel.UserViewModel
import com.google.firebase.firestore.auth.User


class EditProfileActivity : AppCompatActivity() {
    companion object {
        private const val IMAGE_REQUEST = 111
        private const val READ_STORAGE_PERMISSION_CODE = 222
    }

    private val binding by lazy { ActivityEditProfileBinding.inflate(layoutInflater) }
    private val userViewModel: UserViewModel by viewModels()
    private var imageUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initViews()
        initListeners()
    }

    private fun initViews() {
        userViewModel.currentUser.observe(this) { user ->
            user?.let { setViews(it) }
        }
    }

    private fun initListeners() {
        userViewModel.getUserData()
        userViewModel.updateUserResponse.observe(this) {
            if (it.first) {
                onBackPressedDispatcher.onBackPressed()
            }
            Toast.makeText(applicationContext, it.second, Toast.LENGTH_SHORT).show()
        }
        binding.ivAvatar.setOnClickListener {
            if (checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                selectImage()
            } else {
                requestPermission(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    READ_STORAGE_PERMISSION_CODE
                )
            }
        }
        binding.btnComfirm.setOnClickListener {
            val username = binding.edtName.text.toString()
            val phone = binding.edtSdt.text.toString()
            val address = binding.edtAddress.text.toString()
            val user = userViewModel.currentUser.value
            user?.username = username
            user?.phone = phone
            user?.address = address
            imageUri?.let { image ->
                if (user != null) {
                    userViewModel.updateProfile(image, user)
                }
            }
        }
    }

    private fun checkPermission(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission(permission: String, requestCode: Int) {
        ActivityCompat.requestPermissions(this, arrayOf(permission), requestCode)
    }

    private fun selectImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, IMAGE_REQUEST)
    }

    private fun setViews(user: com.example.btl_hdv.model.User) {
        user.image?.let { binding.ivAvatar.loadImage(it) }
        binding.edtName.setText(user.username)
        binding.edtSdt.setText(user.phone)
        binding.edtAddress.setText(user.address)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IMAGE_REQUEST && resultCode == RESULT_OK) {
            imageUri = data?.data
            try {
                binding.ivAvatar.loadImage(imageUri.toString())
            } catch (e: Exception) {
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            READ_STORAGE_PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImage()
                } else {
                    Toast.makeText(this, "Không có quyền truy cập", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}