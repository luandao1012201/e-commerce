package com.example.btl_hdv.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.example.btl_hdv.databinding.ActivityProductDetailBinding
import com.example.btl_hdv.model.CartItem
import com.example.btl_hdv.model.Shoes
import com.example.btl_hdv.ui.fragments.ShopFragment
import com.example.btl_hdv.ultis.format
import com.example.btl_hdv.ultis.loadImage
import com.example.btl_hdv.viewmodel.CartViewModel
import com.example.btl_hdv.viewmodel.ProductViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class ProductDetailActivity : AppCompatActivity() {
    private val binding by lazy { ActivityProductDetailBinding.inflate(layoutInflater) }
    private val productViewModel: ProductViewModel by viewModels()
    private val cartViewModel: CartViewModel by viewModels()
    private var shoes: Shoes? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initViews()
        initListeners()
    }

    private fun initListeners() {
        binding.imgLess.setOnClickListener {
            cartViewModel.changeQuantity(false, this)
        }
        binding.imgMore.setOnClickListener {
            cartViewModel.changeQuantity(true, this)
        }
        cartViewModel.shoesQuantity.observe(this) {
            binding.tvQuantity.text = it.toString()
        }
        binding.btnAddToBasket.setOnClickListener {
            cartViewModel.addToCart(
                CartItem(
                    0,
                    cartViewModel.cartId.value?.id!!,
                    shoes?.ID!!,
                    binding.tvQuantity.text.toString().toInt(),
                    shoes?.price!!
                ), applicationContext
            )
        }
        productViewModel.shoes.observe(this) {
            shoes = it
            if (it != null) {
                setViews(it)
            }
        }
    }

    private fun setViews(shoes: Shoes) {
        binding.ivShoes.loadImage(shoes.images)
        binding.tvDetailName.text = shoes.name
        binding.tvDetailPrice.text = shoes.price.format()
        binding.tvDetailDescription.text = shoes.description
        binding.tvCategory.text = shoes.category.name
    }

    private fun initViews() {
        val intent = intent
        val shoesID = intent.getIntExtra(ShopFragment.SHOES_ID, -1)
        productViewModel.getShoes(shoesID)
        cartViewModel.getCartByUser(Firebase.auth.currentUser?.uid!!)
    }
}