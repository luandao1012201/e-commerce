package com.example.btl_hdv.ui.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.btl_hdv.databinding.ItemCartFragmentBinding
import com.example.btl_hdv.databinding.ItemOrdersActivityBinding
import com.example.btl_hdv.model.Shoes
import com.example.btl_hdv.ultis.format
import com.example.btl_hdv.ultis.loadImage

@SuppressLint("NotifyDataSetChanged")
class ItemOrderDetailAdapter : Adapter<ItemOrderDetailAdapter.ItemCartViewHolder>() {
    private var listShoes = arrayListOf<Shoes>()
    fun setData(list: List<Shoes>) {
        listShoes = list as ArrayList<Shoes>
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemCartViewHolder {
        val itemCartViewHolder = ItemCartViewHolder(
            ItemOrdersActivityBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
        return itemCartViewHolder
    }

    override fun getItemCount(): Int {
        return listShoes.size
    }

    override fun onBindViewHolder(holder: ItemCartViewHolder, position: Int) {
        holder.bind(position)
    }

    @SuppressLint("SetTextI18n")
    inner class ItemCartViewHolder(private val binding: ItemOrdersActivityBinding) :
        ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.tvNameItemCard.text = listShoes[position].name
            binding.tvPrice.text = listShoes[position].price.format()
            binding.ivItemCart.loadImage(listShoes[position].images)
            binding.tvQuantity.text = listShoes[position].quantity.toString()
            binding.tvNameItemCard.isSelected = true
            binding.tvPrice.isSelected = true
        }

    }
}