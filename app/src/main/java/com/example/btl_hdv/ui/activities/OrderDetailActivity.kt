package com.example.btl_hdv.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.example.btl_hdv.R
import com.example.btl_hdv.databinding.ActivityOrderDetailBinding
import com.example.btl_hdv.model.Shoes
import com.example.btl_hdv.ui.adapters.ItemOrderDetailAdapter
import com.example.btl_hdv.viewmodel.CartViewModel

class OrderDetailActivity : AppCompatActivity() {
    private val binding by lazy { ActivityOrderDetailBinding.inflate(layoutInflater) }
    private var adapter: ItemOrderDetailAdapter? = null
    private val cartViewModel: CartViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        adapter = ItemOrderDetailAdapter()
        val cartID = intent.getIntExtra("CartID", -1)
        cartViewModel.getCartItemByCart(cartID)
        cartViewModel.listShoesInCart.observe(this) {
            adapter?.setData(it as List<Shoes>)
            binding.rcvItemOrder.adapter = adapter
        }
    }
}