package com.example.btl_hdv.ui.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.btl_hdv.ui.fragments.AccountFragment
import com.example.btl_hdv.ui.fragments.CartFragment
import com.example.btl_hdv.ui.fragments.ExploreFragment
import com.example.btl_hdv.ui.fragments.ShopFragment

class ViewPagerAdapter(fm: FragmentManager, behavior: Int) : FragmentPagerAdapter(fm, behavior) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ShopFragment()
            1 -> ExploreFragment()
            2 -> CartFragment()
            3 -> AccountFragment()
            else -> ShopFragment()
        }
    }

    override fun getCount(): Int {
        return 4;
    }
}