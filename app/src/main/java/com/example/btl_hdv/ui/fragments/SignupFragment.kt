package com.example.btl_hdv.ui.fragments

import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import com.example.btl_hdv.databinding.FragmentSignupBinding
import com.example.btl_hdv.viewmodel.AuthViewModel


class SignupFragment : Fragment() {
    companion object {
        const val INFO_SIGNUP = "INFO_SIGNUP"
        const val EMAIL_SIGNUP = "EMAIL_SIGNUP"
        const val PASSWORD_SIGNUP = "PASSWORD_SIGNUP"
    }

    private lateinit var binding: FragmentSignupBinding
    private val authViewModel: AuthViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentSignupBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initViews() {
        authViewModel.authSuccess.observe(viewLifecycleOwner) {
            if (it.first) {
                val bundle = Bundle().apply {
                    putString(EMAIL_SIGNUP, binding.edtEmail.text.toString().trim())
                    putString(PASSWORD_SIGNUP, binding.edtPassword.text.toString().trim())
                }
                setFragmentResult(INFO_SIGNUP, bundle)
                parentFragmentManager.popBackStack()
            }
            Toast.makeText(activity?.applicationContext, it.second, Toast.LENGTH_SHORT).show()
        }
    }

    private fun initListeners() {
        binding.btnRegister.setOnClickListener {
            val username = binding.edtUsername.text.toString().trim()
            val email = binding.edtEmail.text.toString().trim()
            val password = binding.edtPassword.text.toString().trim()
            if (validate(username, email, password)) {
                authViewModel.register(username, email, password)
            }
        }
        binding.tvLogin.setOnClickListener {
            parentFragmentManager.popBackStack()
        }
    }

    private fun validate(username: String, email: String, password: String): Boolean {
        if (email.isEmpty()) {
            binding.edtEmail.apply {
                error = "Cannot be empty"
                requestFocus()
            }
            return false
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            binding.edtEmail.apply {
                error = "Email is incorrect"
                requestFocus()
            }
            return false
        }
        if (password.isEmpty()) {
            binding.edtPassword.apply {
                error = "Cannot be empty"
                requestFocus()
            }
            return false
        }
        if (password.length < 8 || password.length > 16) {
            binding.edtPassword.apply {
                error = "Password must be between 8-16 characters"
                requestFocus()
            }
            return false
        }
        val patternNum = ".*\\d.*".toRegex()
        val patternUpperCase = ".*\\w.*".toRegex()
        if (!password.matches(patternNum)) {
            binding.edtPassword.apply {
                error = "Password must have at least 1 number"
                requestFocus()
            }
            return false
        }
        if (!password.matches(patternUpperCase)) {
            binding.edtPassword.apply {
                error = "Password must have at least 1 uppercase letter"
                requestFocus()
            }
            return false
        }
        if (password.contains(" ")) {
            binding.edtPassword.apply {
                error = "Password must not contain spaces"
                requestFocus()
            }
            return false
        }
        return true
    }
}