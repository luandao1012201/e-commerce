package com.example.btl_hdv.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import com.example.btl_hdv.R
import com.example.btl_hdv.databinding.ActivityOrderListBinding
import com.example.btl_hdv.ui.adapters.ItemOrderAdapter
import com.example.btl_hdv.viewmodel.PaymentViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class OrderListActivity : AppCompatActivity() {
    private val binding by lazy { ActivityOrderListBinding.inflate(layoutInflater) }
    private var adapter: ItemOrderAdapter? = null
    private val paymentViewModel: PaymentViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initViews()
        initListeners()
    }

    private fun initViews() {
        adapter = ItemOrderAdapter()
        binding.rv.adapter = adapter
        Firebase.auth.currentUser?.uid?.let { paymentViewModel.getOrder(it) }
    }

    private fun initListeners() {
        paymentViewModel.listOrderResponse.observe(this) {
            adapter?.setData(it)
        }
        adapter?.setOnClick {
            val intent = Intent(this, OrderDetailActivity::class.java)
            intent.putExtra("CartID", it)
            startActivity(intent)
        }
    }
}