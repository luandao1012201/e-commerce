package com.example.btl_hdv.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.btl_hdv.R
import com.example.btl_hdv.databinding.ItemExploreFragmentBinding
import com.example.btl_hdv.model.Category
import com.example.btl_hdv.ultis.loadImage

class ItemExploreAdapter : Adapter<ItemExploreAdapter.ItemExploreViewHolder>() {
    private var listItem = arrayListOf<Category>()
    private var onClickCallback: ((id: Int) -> Unit)? = null
    fun setData(list: ArrayList<Category>) {
        listItem = list
        notifyDataSetChanged()
    }

    fun setOnClick(onClickCallback: ((id: Int) -> Unit)? = null){
        this.onClickCallback = onClickCallback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemExploreViewHolder {
        val itemExploreViewHolder = ItemExploreViewHolder(
            ItemExploreFragmentBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
        itemExploreViewHolder.bindListeners()
        return itemExploreViewHolder
    }

    override fun getItemCount(): Int = listItem.size

    override fun onBindViewHolder(holder: ItemExploreViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ItemExploreViewHolder(private val binding: ItemExploreFragmentBinding) :
        ViewHolder(binding.root) {
        fun bind(position: Int) {
            when (position % 6) {
                0 -> binding.root.background =
                    ContextCompat.getDrawable(binding.root.context, R.drawable.item_explore_bg_1)

                1 -> binding.root.background =
                    ContextCompat.getDrawable(binding.root.context, R.drawable.item_explore_bg_2)

                2 -> binding.root.background =
                    ContextCompat.getDrawable(binding.root.context, R.drawable.item_explore_bg_3)

                3 -> binding.root.background =
                    ContextCompat.getDrawable(binding.root.context, R.drawable.item_explore_bg_4)

                4 -> binding.root.background =
                    ContextCompat.getDrawable(binding.root.context, R.drawable.item_explore_bg_5)

                5 -> binding.root.background =
                    ContextCompat.getDrawable(binding.root.context, R.drawable.item_explore_bg_6)
            }
            val item = listItem[position]
            binding.tvName.text = item.name
            binding.ivItem.loadImage(item.image)
        }

        fun bindListeners() {
            binding.root.setOnClickListener {
                onClickCallback?.invoke(listItem[adapterPosition].id)
            }
        }
    }
}