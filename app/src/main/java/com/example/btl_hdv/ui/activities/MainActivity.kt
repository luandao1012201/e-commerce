package com.example.btl_hdv.ui.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import com.example.btl_hdv.R
import com.example.btl_hdv.databinding.ActivityMainBinding
import com.example.btl_hdv.ui.fragments.LoginFragment
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity() {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val currentUser = Firebase.auth.currentUser
        if (currentUser == null) {
            supportFragmentManager.commit {
                add(R.id.fragment_container_view, LoginFragment())
                addToBackStack(null)
            }
        } else {
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if(supportFragmentManager.backStackEntryCount == 0) finish()
    }
}