package com.example.btl_hdv.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import com.example.btl_hdv.ui.adapters.ItemShopAdapter
import com.example.btl_hdv.R
import com.example.btl_hdv.databinding.FragmentShopBinding
import com.example.btl_hdv.model.CartItem
import com.example.btl_hdv.model.Shoes
import com.example.btl_hdv.ui.activities.HomeActivity
import com.example.btl_hdv.ui.activities.ProductDetailActivity
import com.example.btl_hdv.ui.activities.SearchActivity
import com.example.btl_hdv.viewmodel.CartViewModel
import com.example.btl_hdv.viewmodel.ProductViewModel

class ShopFragment : Fragment() {
    companion object {
        const val SHOES_ID = "SHOES_ID"
    }

    private lateinit var binding: FragmentShopBinding
    private var itemShopAdapter: ItemShopAdapter? = null
    private val cartViewModel: CartViewModel by activityViewModels()
    private val productViewModel: ProductViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentShopBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initViews() {
        itemShopAdapter = ItemShopAdapter()
        productViewModel.listShoes.observe(this.viewLifecycleOwner) {
            if (it != null) {
                itemShopAdapter?.setData(it)
            }
        }
        binding.rcvItem.adapter = itemShopAdapter
    }

    private fun initListeners() {
        productViewModel.getAllProduct()
        itemShopAdapter?.setItemOnClick {
            val intent = Intent(Intent(activity, ProductDetailActivity::class.java))
            intent.putExtra(SHOES_ID, it)
            activity?.startActivity(intent)
        }
        cartViewModel.cartId.observe(this.viewLifecycleOwner) { cart ->
            itemShopAdapter?.addToCart {
                cartViewModel.addToCart(
                    CartItem(0, cart?.id!!, it.ID, 1, it.price), this.requireContext()
                )
            }
        }
        binding.tvSearch.setOnClickListener {
            activity?.startActivity(Intent(activity, SearchActivity::class.java))
        }
    }
}