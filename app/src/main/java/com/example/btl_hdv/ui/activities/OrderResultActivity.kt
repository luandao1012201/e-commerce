package com.example.btl_hdv.ui.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.viewModelScope
import com.example.btl_hdv.R
import com.example.btl_hdv.databinding.ActivityOrderResultBinding
import com.example.btl_hdv.model.Order
import com.example.btl_hdv.model.Payment
import com.example.btl_hdv.model.Shipment
import com.example.btl_hdv.ui.fragments.CartFragment
import com.example.btl_hdv.ui.fragments.CartFragment.Companion.CART
import com.example.btl_hdv.ui.fragments.CartFragment.Companion.ORDER_INFO
import com.example.btl_hdv.viewmodel.CartViewModel
import com.example.btl_hdv.viewmodel.PaymentViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import vn.zalopay.sdk.ZaloPaySDK
import java.util.Calendar

class OrderResultActivity : AppCompatActivity() {
    private val binding by lazy { ActivityOrderResultBinding.inflate(layoutInflater) }
    private var name = ""
    private var phone = ""
    private var address = ""
    private var price = 0f
    private var cart = -1
    private val sharedPreferences by lazy { getSharedPreferences("Preferences", MODE_PRIVATE) }
    private val paymentViewModel: PaymentViewModel by viewModels()
    private val cartViewModel: CartViewModel by viewModels()
    private var calendar = Calendar.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initViews()
        initListener()
    }

    private fun initViews() {
        val data = intent?.data
        val success = intent.getBooleanExtra("success", false)
        ZaloPaySDK.getInstance().onResult(intent)
        name = sharedPreferences.getString(CartFragment.NAME, "").toString()
        phone = sharedPreferences.getString(CartFragment.PHONE, "").toString()
        address = sharedPreferences.getString(CartFragment.ADDRESS, "").toString()
        price = sharedPreferences.getFloat(CartFragment.PRICE, 0F)
        cart = sharedPreferences.getInt(CART, -1)
        setCalendar()
        val vnp_ResponseCode = data?.getQueryParameter("vnp_ResponseCode")
        if (vnp_ResponseCode == "00" || success) {
            saveOrder()
            cartViewModel.updatePay(cart)
            setViews(
                R.drawable.icon_order_success,
                "Thanh toán thành công",
                "Đơn hàng của bạn đã được thanh toán. Kiểm tra đơn hàng ở Quản lý đơn hàng"
            )
            binding.cvTrackOrder.visibility = View.VISIBLE
        } else {
            setViews(
                R.drawable.icon_order_fail,
                "Thanh toán thất bại",
                "Đơn hàng của bạn chưa được thanh toán. Vui lòng kiểm tra lại thông tin thanh toán"
            )
            binding.cvTrackOrder.visibility = View.GONE
        }
    }

    private fun initListener() {
        binding.cvBackToHome.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setViews(image: Int, title: String, message: String) {
        binding.ivOrderSuccess.setImageResource(image)
        binding.tvOrderSuccess.text = title
        binding.tvMessage.text = message
    }

    private fun setCalendar() {
        calendar.apply {
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }
    }

    private fun saveOrder() {
        val payment = Payment(0, calendar.timeInMillis, price)
        paymentViewModel.addPayment(payment)
        val shipment = Shipment(0, name, phone, address, price)
        paymentViewModel.addShipment(shipment)
        paymentViewModel.paymentId.observe(this) { paymentId ->
            paymentViewModel.shipmentId.observe(this) { shipmentId ->
                val uid = Firebase.auth.currentUser?.uid
                uid?.let {
                    if (shipmentId != null) {
                        if (paymentId != null) {
                            val order =
                                Order(0, it, shipmentId, cart, paymentId, calendar.timeInMillis)
                            paymentViewModel.addOrder(order)
                        }
                    }
                }
            }
        }
    }
}