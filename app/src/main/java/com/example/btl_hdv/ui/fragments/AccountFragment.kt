package com.example.btl_hdv.ui.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.btl_hdv.databinding.FragmentAccountBinding
import com.example.btl_hdv.ui.activities.ChangePasswordActivity
import com.example.btl_hdv.ui.activities.EditProfileActivity
import com.example.btl_hdv.ui.activities.MainActivity
import com.example.btl_hdv.ui.activities.OrderListActivity
import com.example.btl_hdv.ultis.loadImage
import com.example.btl_hdv.viewmodel.UserViewModel
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class AccountFragment : Fragment() {
    private lateinit var binding: FragmentAccountBinding
    private val userViewModel: UserViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAccountBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initViews() {
        userViewModel.currentUser.observe(this.viewLifecycleOwner) { user ->
            user?.let { setViews(it) }
        }
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            for (userInfo in user.providerData) {
                if (userInfo.providerId == EmailAuthProvider.PROVIDER_ID) {
                    binding.tvChangePassword.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        userViewModel.getUserData()
    }

    private fun setViews(user: com.example.btl_hdv.model.User) {
        user.image?.let { binding.ivAvatar.loadImage(it) }
        binding.tvUsername.text = user.username
        binding.tvEmail.text = user.email
    }

    private fun initListeners() {
        userViewModel.getUserData()
        binding.btnLogout.setOnClickListener {
            Firebase.auth.signOut()
            activity?.startActivity(Intent(activity, MainActivity::class.java))
            activity?.finish()
        }
        binding.tvOrders.setOnClickListener {
            activity?.startActivity(Intent(activity, OrderListActivity::class.java))
        }
        binding.tvMyDetails.setOnClickListener {
            activity?.startActivity(Intent(activity, EditProfileActivity::class.java))
        }
        binding.tvChangePassword.setOnClickListener {
            activity?.startActivity(Intent(activity, ChangePasswordActivity::class.java))
        }
    }
}