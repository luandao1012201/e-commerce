package com.example.btl_hdv.ui.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.btl_hdv.databinding.ItemListOrderBinding
import com.example.btl_hdv.model.OrderResponse
import com.example.btl_hdv.ultis.format
import com.example.btl_hdv.ultis.formatToDate

class ItemOrderAdapter : Adapter<ItemOrderAdapter.ItemOrderViewHolder>() {
    private var listItem = listOf<OrderResponse>()
    private var callbackOnClick: ((id: Int) -> Unit)? = null
    fun setData(list: List<OrderResponse>) {
        listItem = list
        notifyDataSetChanged()
    }

    fun setOnClick(callback: ((id: Int) -> Unit)? = null) {
        callbackOnClick = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemOrderViewHolder {
        val itemOrderViewHolder = ItemOrderViewHolder(
            ItemListOrderBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
        itemOrderViewHolder.bindListeners()
        return itemOrderViewHolder
    }

    override fun getItemCount(): Int = listItem.size
    override fun onBindViewHolder(holder: ItemOrderViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ItemOrderViewHolder(private val binding: ItemListOrderBinding) :
        ViewHolder(binding.root) {
        fun bind(position: Int) {
            val order = listItem[position]
            binding.tvIdOrder.text = order.orderDate.toString()
            binding.tvDate.text = order.orderDate.formatToDate()
            binding.tvAddress.text = order.shipment.address
            binding.tvNumberItem.text = "${order.quantity} sản phẩm"
            binding.tvPrice.text = order.payment.total.format()
            binding.tvStatus.text = "Đang chuẩn bị"
        }

        fun bindListeners() {
            binding.root.setOnClickListener {
                callbackOnClick?.invoke(listItem[adapterPosition].cart)
            }
        }
    }
}