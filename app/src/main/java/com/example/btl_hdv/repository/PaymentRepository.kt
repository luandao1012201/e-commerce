package com.example.btl_hdv.repository

import com.example.btl_hdv.model.Payment
import com.example.btl_hdv.network.ApiBuilder
import com.google.protobuf.Api

class PaymentRepository {
    suspend fun getAllProvince() = ApiBuilder.paymentApiService.getProvince()
    suspend fun getAllDistrict(code: Int) = ApiBuilder.paymentApiService.getDistrict(code)
    suspend fun getAllWard(code: Int) = ApiBuilder.paymentApiService.getWard(code)
    suspend fun addPayment(payment: Payment) = ApiBuilder.paymentApiService.addPayment(payment)
    suspend fun getPayment(id: Int) = ApiBuilder.paymentApiService.getPayment(id)
}