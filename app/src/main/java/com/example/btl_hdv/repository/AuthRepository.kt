package com.example.btl_hdv.repository

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.example.btl_hdv.R
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.OAuthProvider
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await

class AuthRepository() {
    private val auth = Firebase.auth
    private val store = Firebase.firestore

    suspend fun register(username: String, email: String, password: String): Pair<Boolean, String> {
        var success: Boolean
        var message: String
        if (isEmailRegistered(email)) {
            success = false
            message = "Email already exists"
        } else {
            try {
                auth.createUserWithEmailAndPassword(email, password).await()
                val user = auth.currentUser
                user?.sendEmailVerification()?.await()
                user?.let {
                    val userData = hashMapOf(
                        "username" to username,
                        "email" to email,
                        "image" to "https://firebasestorage.googleapis.com/v0/b/btl-hdv-c425d.appspot.com/o/icons8-user-100.png?alt=media&token=f0a6a5f5-56e1-4006-8332-7fa3e07efe44"
                    )
                    store.collection("users").document(user.uid).set(userData).await()
                }
                success = true
                message = "You have successfully registered. Please verify your email"
            } catch (e: java.lang.Exception) {
                success = false
                message = "You have failed to register"
            }
        }
        return Pair(success, message)
    }

    suspend fun login(email: String, password: String): Pair<Boolean, String> {
        var success: Boolean
        var message: String
        try {
            auth.signInWithEmailAndPassword(email, password).await()
            val verification = auth.currentUser?.isEmailVerified
            if (verification == true) {
                success = true
                message = "Successful login"
            } else {
                success = false
                message = "Please verify your email"
            }
        } catch (e: Exception) {
            success = false
            message = "Login failed"
        }
        return Pair(success, message)
    }

    suspend fun loginWithGoogle(
        idToken: String, username: String, email: String, image: Uri
    ): Pair<Boolean, String> {
        var success: Boolean
        var message: String
        try {
            val credential = GoogleAuthProvider.getCredential(idToken, null)
            val authResult = auth.signInWithCredential(credential).await()
            val user = authResult.user
            val isNewUser = authResult.additionalUserInfo?.isNewUser
            if (isNewUser == true) {
                val userData = hashMapOf(
                    "username" to user?.displayName,
                    "email" to user?.email,
                    "image" to user?.photoUrl,
                )
                store.collection("users").document(user?.uid ?: "").set(userData).await()
            }
            success = true
            message = "Successful login"
        } catch (e: Exception) {
            success = false
            message = "Login failed"
        }
        return Pair(success, message)
    }

    private suspend fun isEmailRegistered(email: String): Boolean {
        val result = auth.fetchSignInMethodsForEmail(email).await()
        return !result.signInMethods.isNullOrEmpty()
    }

    //    fun signOut(activity: Activity) {
//        mAuth.signOut()
//        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//            .requestIdToken(activity.getString(R.string.default_web_client_id))
//            .requestEmail()
//            .build()
//        val googleSignInClient = GoogleSignIn.getClient(activity, gso)
//        googleSignInClient.signOut()
//    }
    suspend fun loginWithFacebook(loginResult: LoginResult): Pair<Boolean, String> {
        var success: Boolean
        var message: String
        try {
            val credential = FacebookAuthProvider.getCredential(loginResult.accessToken.token)
            var image = ""
            val request = GraphRequest.newMeRequest(
                loginResult.accessToken
            ) { jsonObject, _ ->
                image =
                    jsonObject?.getJSONObject("picture")?.getJSONObject("data")?.getString("url")
                        .toString()
            }
            val parameters = Bundle()
            parameters.putString("fields", "picture.type(large)")
            request.parameters = parameters
            request.executeAsync()
            val authResult = auth.signInWithCredential(credential).await()
            val user = authResult.user
            val isNewUser = authResult.additionalUserInfo?.isNewUser
            if (isNewUser == true) {
                val userData = hashMapOf(
                    "username" to user?.displayName,
                    "email" to user?.email,
                    "image" to image,
                )
                store.collection("users").document(user?.uid ?: "").set(userData)
            }
            success = true
            message = "Successful login"
        } catch (e: Exception) {
            success = false
            message = e.message.toString()
        }
        return Pair(success, message)
    }

    suspend fun loginWithTwitter(activity: Activity): Pair<Boolean, String> {
        var success: Boolean
        var message: String
        try {
            val provider = OAuthProvider.newBuilder("twitter.com")
            provider.addCustomParameter("lang", "vn")
            val pendingResultTask = auth.pendingAuthResult
            if (pendingResultTask != null) {
                pendingResultTask.await()
                success = true
                message = "Successful login"
            } else {
                val authResult =
                    auth.startActivityForSignInWithProvider(activity, provider.build()).await()
                val user = authResult.user
                val isNewUser = authResult.additionalUserInfo?.isNewUser
                if (isNewUser == true) {
                    val userData = hashMapOf(
                        "username" to user?.displayName,
                        "email" to user?.email,
                        "image" to user?.photoUrl
                    )
                    store.collection("users").document(user?.uid ?: "").set(userData)
                }
                success = true
                message = "Successful login"
            }
        } catch (e: Exception) {
            success = false
            message = e.message.toString()
        }
        return Pair(success, message)
    }
}