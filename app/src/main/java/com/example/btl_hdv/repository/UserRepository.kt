package com.example.btl_hdv.repository

import android.net.Uri
import android.util.Log
import com.example.btl_hdv.model.User
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.coroutines.tasks.await

class UserRepository {
    private val store = Firebase.firestore
    private val auth = Firebase.auth
    private val storageRef: StorageReference = FirebaseStorage.getInstance().reference
    suspend fun getDataUser(): User? {
        val user: User?
        try {
            user = store.collection("users")
                .document(auth.currentUser?.uid.toString())
                .get()
                .await()
                .toObject(User::class.java)
            return user
        } catch (e: Exception) {
            Log.e("test123", e.message.toString())
        }
        return null
    }

    suspend fun updateUser(user: User): Pair<Boolean, String> {
        var success: Boolean
        var message: String
        try {
            val updates = hashMapOf(
                "email" to user.email.toString(),
                "username" to user.username.toString(),
                "image" to user.image.toString(),
                "phone" to user.phone.toString(),
                "address" to user.address.toString()
            )
            store.collection("users").document(auth.currentUser?.uid.toString())
                .update(updates as Map<String, Any>).await()
            success = true
            message = "Cập nhật thông tin thành công"
        } catch (e: java.lang.Exception) {
            success = false
            message = "Cập nhật thông tin thất bại"
        }
        return Pair(success, message)
    }

    suspend fun uploadImage(imageUri: Uri): String {
        var url = ""
        try {
            val imagesRef: StorageReference = storageRef.child("${auth.currentUser?.uid}.jpg")
            imagesRef.putFile(imageUri).await()
            url = imagesRef.downloadUrl.await().toString()
        } catch (e: Exception) {
            Log.d("test123", e.message.toString())
        }
        return url
    }

    suspend fun changePassword(oldPassword: String, newPassword: String): Pair<Boolean, String> {
        var success: Boolean
        var message: String
        val currentUser = auth.currentUser
        val credential = EmailAuthProvider.getCredential(currentUser?.email ?: "", oldPassword)
        try {
            currentUser?.reauthenticate(credential)?.await()
            currentUser?.updatePassword(newPassword)?.await()
            success = true
            message = "Đổi mật khẩu thành công"
        } catch (e: Exception) {
            success = false
            message = "Đổi mật khẩu thất bại"
        }
        return Pair(success, message)
    }
}