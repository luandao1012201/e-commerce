package com.example.btl_hdv.repository

import com.example.btl_hdv.model.CartItem
import com.example.btl_hdv.network.ApiBuilder

class CartRepository {
    suspend fun getCartByUser(uid: String) = ApiBuilder.cartApiService.getCartByUser(uid)
    suspend fun addToCart(cartItem: CartItem) = ApiBuilder.cartApiService.addToCart(cartItem)
    suspend fun getCartItemByCart(cartId: Int) = ApiBuilder.cartApiService.getCartItemByCart(cartId)
    suspend fun updatePay(cartId: Int) = ApiBuilder.cartApiService.updatePay(cartId)
    suspend fun deleteItem(cartId: Int, productId: Int) =
        ApiBuilder.cartApiService.deleteItem(cartId, productId)
}