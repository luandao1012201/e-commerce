package com.example.btl_hdv.repository

import com.example.btl_hdv.model.Shipment
import com.example.btl_hdv.network.ApiBuilder

class ShipmentRepository {
    suspend fun addShipment(shipment: Shipment) =
        ApiBuilder.shipmentApiService.addShipment(shipment)

    suspend fun getShipment(id: Int) = ApiBuilder.shipmentApiService.getShipment(id)
}