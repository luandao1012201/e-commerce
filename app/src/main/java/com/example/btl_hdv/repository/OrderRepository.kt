package com.example.btl_hdv.repository

import com.example.btl_hdv.model.Order
import com.example.btl_hdv.network.ApiBuilder

class OrderRepository {
    suspend fun addOrder(order: Order) = ApiBuilder.orderApiService.addOrder(order)
    suspend fun getOrder(id: String) = ApiBuilder.orderApiService.getOrder(id)
}