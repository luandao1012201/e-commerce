package com.example.btl_hdv.repository

import com.example.btl_hdv.network.ApiBuilder

class ShoesRepository {
    suspend fun getAllProduct() = ApiBuilder.shoesApiService.getAllShoes()
    suspend fun getShoes(id: Int) = ApiBuilder.shoesApiService.getShoes(id)
    suspend fun getAllCategory() = ApiBuilder.shoesApiService.getAllCategory()
    suspend fun getShoesByCategory(category: Int) =
        ApiBuilder.shoesApiService.getShoesByCategory(category)

    suspend fun searchShoes(key: String) = ApiBuilder.shoesApiService.searchShoes(key)
}