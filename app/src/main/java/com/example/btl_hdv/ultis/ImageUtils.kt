package com.example.btl_hdv.ultis

import android.annotation.SuppressLint
import android.widget.ImageView
import com.bumptech.glide.Glide
import java.text.DecimalFormat
import java.text.SimpleDateFormat


@SuppressLint("Range")
fun ImageView.loadImage(url: String) {
    Glide.with(context)
        .load(url)
        .into(this)
}

fun Float.format(): String {
    val decimalFormat = DecimalFormat("#,###,###")
    return decimalFormat.format(this) + " VND"
}

fun Long.formatToDate(): String {
    return SimpleDateFormat("dd-MM-yyyy HH:mm").format(this)
}