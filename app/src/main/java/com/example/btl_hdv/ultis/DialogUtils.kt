package com.example.btl_hdv.ultis

import android.app.AlertDialog
import android.app.Dialog
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.widget.EditText
import androidx.core.view.marginStart
import com.example.btl_hdv.R
import com.example.btl_hdv.databinding.BottomSheetSelectAddressBinding
import com.example.btl_hdv.databinding.BottomSheetSelectPaymentMethodBinding
import com.example.btl_hdv.viewmodel.PaymentViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog

fun Dialog.showBottomSheetPaymentMethod(callback: ((method: String, image: Int) -> Unit)? = null) {
    val binding = BottomSheetSelectPaymentMethodBinding.inflate(LayoutInflater.from(this.context))
    val bottomSheet = BottomSheetDialog(this.context)
    bottomSheet.setContentView(binding.root)
    bottomSheet.show()
    binding.tvSelectMethod.setOnClickListener {
        bottomSheet.dismiss()
        this.show()
    }
    binding.tvMomo.setOnClickListener {
        callback?.invoke("Momo", R.drawable.logo_momo)
        bottomSheet.dismiss()
        this.show()
    }
    binding.tvVnpay.setOnClickListener {
        callback?.invoke("VNPay", R.drawable.logo_vnpay)
        bottomSheet.dismiss()
        this.show()
    }
    binding.tvZalopay.setOnClickListener {
        callback?.invoke("Zalo pay", R.drawable.logo_zalopay)
        bottomSheet.dismiss()
        this.show()
    }
}

fun Dialog.showBottomSheetAddress(
    paymentViewModel: PaymentViewModel,
    callback: ((address: String) -> Unit)? = null
) {
    val binding = BottomSheetSelectAddressBinding.inflate(LayoutInflater.from(this.context))
    val bottomSheet = BottomSheetDialog(this.context)
    bottomSheet.setContentView(binding.root)
    bottomSheet.show()
    paymentViewModel.getAllProvince()
    binding.tvProvince.setOnClickListener {
        var listProvince = arrayOf<String>()
        paymentViewModel.listProvince.value?.forEach { listProvince += it.name }
        AlertDialog.Builder(this.context).setData("Chọn tỉnh/ thành phố", listProvince) {
            binding.tvProvince.text = listProvince[it]
            paymentViewModel.listProvince.value?.get(it)?.let { province ->
                paymentViewModel.getAllDistrict(province.code)
            }
        }
    }
    binding.tvDistrict.setOnClickListener {
        var listDistrict = arrayOf<String>()
        paymentViewModel.listDistrict.value?.forEach { listDistrict += it.name }
        AlertDialog.Builder(this.context).setData("Chọn huyện/ quận", listDistrict) {
            binding.tvDistrict.text = listDistrict[it]
            paymentViewModel.listDistrict.value?.get(it)?.let { district ->
                paymentViewModel.getAllWard(district.code)
            }
        }
    }
    binding.tvWard.setOnClickListener {
        var listWard = arrayOf<String>()
        paymentViewModel.listWard.value?.forEach { listWard += it.name }
        AlertDialog.Builder(this.context).setData("Chọn huyện/ quận", listWard) {
            binding.tvWard.text = listWard[it]
        }
    }
    binding.tvSelectAddress.setOnClickListener {
        bottomSheet.dismiss()
        this.show()
    }
    binding.cvConfirm.setOnClickListener {
        val province = binding.tvProvince.text.toString()
        val district = binding.tvDistrict.text.toString()
        val ward = binding.tvWard.text.toString()
        val address = binding.edtAddress.text.toString()
        callback?.invoke("$address, $ward, $district, $province")
        bottomSheet.dismiss()
        this.show()
    }
}

fun Dialog.showInputPhone(callback: ((phone: String) -> Unit)?) {
    val builder = AlertDialog.Builder(this.context)
    builder.setTitle("Nhập số điện thoại")

    val input = EditText(this.context)
    input.inputType = InputType.TYPE_CLASS_TEXT
    builder.setView(input)
    builder.setPositiveButton("OK") { dialog, which ->
        val inputData = input.text.toString()
        callback?.invoke(inputData)
    }
    builder.setNegativeButton("Hủy bỏ") { dialog, which -> dialog.cancel() }
    builder.show()
}

fun AlertDialog.Builder.setData(
    title: String,
    array: Array<String>,
    callback: ((position: Int) -> Unit)?
) {
    var checked = -1
    this.setTitle(title)
    this.setNegativeButton("Hủy") { _, _ -> }
    this.setPositiveButton("Chọn") { dialog, _ ->
        callback?.invoke(checked)
    }
    this.setSingleChoiceItems(array, -1) { dialog, index ->
        checked = index
    }
    this.create().show()
}