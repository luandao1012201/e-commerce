package com.example.btl_hdv.ultis

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.example.btl_hdv.R
import com.example.btl_hdv.databinding.BottomSheetPaymentBinding
import com.example.btl_hdv.viewmodel.PaymentViewModel
import com.example.btl_hdv.viewmodel.UserViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog

fun Context.showBottomSheet(
    userViewModel: UserViewModel,
    paymentViewModel: PaymentViewModel,
    totalPrice: Float,
    callback: ((method: String, address: String, phone: String) -> Unit)? = null
) {
    val binding = BottomSheetPaymentBinding.inflate(LayoutInflater.from(this))
    val bottomSheet = BottomSheetDialog(this)
    binding.root.background = ContextCompat.getDrawable(this, R.drawable.bottom_sheet_payment_bg)
    bottomSheet.setContentView(binding.root)
    binding.tvPrice.text = totalPrice.format()
    binding.tvSelectName.text = userViewModel.currentUser.value?.username.toString()
    bottomSheet.show()
    binding.tvSelectAddress.isSelected = true
    binding.tvMethod.setOnClickListener {
        bottomSheet.dismiss()
        bottomSheet.showBottomSheetPaymentMethod { method, image ->
            binding.tvMethod.text = method
            binding.ivMethod.setImageResource(image)
        }
    }
    binding.tvSelectAddress.setOnClickListener {
        bottomSheet.dismiss()
        bottomSheet.showBottomSheetAddress(paymentViewModel) {
            binding.tvSelectAddress.text = it
        }
    }
    binding.cv.setOnClickListener {
        bottomSheet.dismiss()
        callback?.invoke(
            binding.tvMethod.text.toString(),
            binding.tvSelectAddress.text.toString(),
            binding.tvSelectPhone.text.toString()
        )
    }
    binding.tvSelectPhone.setOnClickListener {
        bottomSheet.showInputPhone {
            binding.tvSelectPhone.text = it
        }
    }
}